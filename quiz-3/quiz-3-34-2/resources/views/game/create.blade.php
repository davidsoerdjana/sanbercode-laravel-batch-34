@extends('layouts.master')

@section('title')
Form Game Baru
@endsection

@section('content')

<form method="POST" action="/game">
  @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name">
  </div>

  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Gameplay</label>
    <input type="text" class="form-control" name="gameplay">
  </div>

  @error('gameplay')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Developer</label>
    <input type="text" class="form-control" name="developer">
  </div>

  @error('developer')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Year</label>
    <input type="text" class="form-control" name="year">
  </div>

  @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection