@extends('layouts.master')

@section('title')
List Game
@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>
@endpush

@section('content')

<a href="/game/create/" class="btn btn-primary mb-3">Tambah Game</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Gameplay</th>
      <th scope="col">Developer</th>
      <th scope="col">Year</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @forelse ($game as $key => $item)
      <tr>
        <th>{{$key+1}}</th>
        <td>{{$item->name}}</td>
        <td>{{$item->gameplay}}</td>
        <td>{{$item->developer}}</td>
        <td>{{$item->year}}</td>
        <td>
          <form action="/game/{{$item->id}}" method="post">
          <a href="/game/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
          <a href="/game/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
          </form>
        </td>
      </tr>
      @empty
        <tr>
          <td>Daftar game masih kosong</td>
        </tr>
    @endforelse
    
  </tbody>
</table>

@endsection