@extends('layouts.master')

@section('title')
Edit Game
@endsection

@section('content')

<form method="POST" action="/game/{{$game->id}}">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" value="{{$game->name}}" name="name">
  </div>

  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Gameplay</label>
    <input type="text" class="form-control" value="{{$game->gameplay}}" name="gameplay">
  </div>

  @error('gameplay')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Developer</label>
    <textarea name="developer" class="form-control" cols="1" rows="3">{{$game->developer}}</textarea>
  </div>

  @error('developer')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Year</label>
    <textarea name="year" class="form-control" cols="1" rows="3">{{$game->year}}</textarea>
  </div>

  @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection