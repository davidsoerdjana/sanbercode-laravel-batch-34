@extends('layouts.master')

@section('title')
Detail Game {{$game->id}}
@endsection

@section('content')

<h2 class="text-primary">{{$game->name}} ({{$game->year}})</h2>
<small>Developer : {{$game->developer}}</small><br><br>
<h4>Gameplay</h4>
<p>{{$game->gameplay}}</p>
<h4>Platform</h4>
@forelse ($platform as $key=>$value)
  <span class="badge badge-primary">{{$value->name}}</span>
  @empty
    Tidak ada platform
@endforelse

@endsection