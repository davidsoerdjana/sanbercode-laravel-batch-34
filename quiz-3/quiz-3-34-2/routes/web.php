<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('erd');
});

// CRUD GameController

// Create
Route::get('/game/create', 'GameController@create'); 
Route::post('/game', 'GameController@store'); 

// Read
Route::get('/game', 'GameController@index'); 
Route::get('/game/{game_id}', 'GameController@show'); 

// Update
Route::get('/game/{game_id}/edit', 'GameController@edit'); 
Route::put('/game/{game_id}', 'GameController@update'); 

// Delete
Route::delete('/game/{game_id}/', 'GameController@destroy');
