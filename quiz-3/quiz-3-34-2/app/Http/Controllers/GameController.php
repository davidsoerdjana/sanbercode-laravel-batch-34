<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create() 
    {
        return view('game.create');
    }

    public function store(request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ],
        [
            'name.required' => 'alamat harus diisi!',
            'gameplay.required' => 'umur harus diisi!',
            'developer.required' => 'pengembang harus diisi!',
            'year.required' => 'tahun harus diisi!'
        ]);

        DB::table('game')->insert(
            [
                'name' => $request['name'],
                'gameplay' => $request['gameplay'],
                'developer' => $request['developer'],
                'year' => $request['year']
            ]
        );

        return redirect('/game');
    }

    public function index()
    {
        $game = DB::table('game')->get();
 
        return view('game.index', compact('game'));
    }

    public function show($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        $platform = DB::table('platform')->where('game_id', $id)->get();
        return view('game.show', compact('game', 'platform'));
    }
    
    public function edit($id)
    {
        $game = DB::table('game')->where('id', $id)->first();

        return view('game.edit', compact('game'));
    }

    public function update($id, request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ],
        [
            'name.required' => 'alamat harus diisi!',
            'gameplay.required' => 'umur harus diisi!',
            'developer.required' => 'pengembang harus diisi!',
            'year.required' => 'tahun harus diisi!'
        ]);

        DB::table('game')
            ->where('id', $id)
            ->update(
            [
                'name' => $request['name'],
                'gameplay' => $request['gameplay'],
                'developer' => $request['developer'],
                'year' => $request['year']
            ]
        );

        return redirect('/game');
    }
    
    public function destroy($id)
    {
        DB::table('game')->where('id', $id)->delete();

        return redirect('/game');
    }
}
