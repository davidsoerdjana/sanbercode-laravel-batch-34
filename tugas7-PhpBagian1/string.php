<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>String PHP</title>
</head>

<body>
  <h1>Berlatih String PHP</h1>

  <?php
  /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut!
            Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */
  echo "<h3> Soal No 1</h3>";

  $first_sentence = "Hello PHP!";
  // jawaban
  echo "First Sentence = " . $first_sentence . "<br>";
  echo "Panjang String = " . strlen($first_sentence) . "<br>"; // output: Panjang String: 10
  echo "Jumlah Kata = " . str_word_count($first_sentence) . "<br>" . "<br>"; // output: Jumlah Kata: 2

  $second_sentence = "I'm ready for the challenges";
  // jawaban
  echo "Second Sentence = " . $second_sentence . "<br>";
  echo "Panjang String = " . strlen($second_sentence) . "<br>"; // output: Panjang String: 28
  echo "Jumlah Kata = " . str_word_count($second_sentence) . "<br>" . "<br>"; // output: Jumlah Kata: 5


  /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
  echo "<h3> Soal No 2</h3>";
  $string2 = "I love PHP";
  echo "<label>String: </label> \"$string2\" <br>";
  echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
  // jawaban
  echo "Kata kedua: " . substr($string2, 2, 4); // output: love 
  echo "<br> Kata Ketiga: " . substr($string2, 6, 4) . "<br>" . "<br>"; // output: PHP


  /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
  echo "<h3> Soal No 3 </h3>";
  $string3 = "PHP is old but sexy!";
  echo "String: \"$string3\" " . "<br>";
  // jawaban
  echo "Mengubah kata: " . str_replace("sexy", "awesome", $string3); // OUTPUT : "PHP is old but awesome"
  ?>

</body>

</html>