{{-- <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h1>Halaman Register</h1> --}}

    {{-- code #6 --}}
    {{-- <form action="/welcome" method="post"> --}}
      
      {{-- @csrf berfungsi untuk token, setiap metode post harus pakai @csrf --}}
      {{-- @csrf  --}}

      {{-- <label>First Name:</label> <br />
      <input type="text" name="first_name" /> <br />
      <br />
      <label>Last Name:</label> <br />
      <input type="text" name="last_name" /> <br />
      <br />    
      <input type="submit" value="Register">
    </form>
</body>
</html> --}}


{{-- code #35 --}}
@extends('layout.master')

@section('title')
Register Form
@endsection

@section('content')
  <form action="/welcome" method="post">
    
    {{-- @csrf berfungsi untuk token, setiap metode post harus pakai @csrf --}}
    @csrf 

    <label>First Name:</label> <br />
    <input type="text" name="first_name" /> <br />
    <br />
    <label>Last Name:</label> <br />
    <input type="text" name="last_name" /> <br />
    <br />
    
    <input type="submit" value="Register">
  </form>
@endsection