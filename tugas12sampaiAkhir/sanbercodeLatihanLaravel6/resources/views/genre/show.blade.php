{{-- #83 --}}
@extends('layout.master')

@section('title')
Detail Genre
@endsection

@section('content')
<h1>{{$genre->nama}}</h1>
<div class="row">
  <div class="col-4">
    @forelse ($genre->film as $item)
        <div class="card">           
          <img src="{{asset('images/'.$item->poster)}}" width="100%" height="300px" alt="...">
          <div class="card-body">
              <h3><b>{{$item->judul}}</b></h3>
              <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
          </div>
        </div>
    @empty
        <h3>Tidak ada film di genre ini</h3>
    @endforelse
  </div>
</div>

@endsection