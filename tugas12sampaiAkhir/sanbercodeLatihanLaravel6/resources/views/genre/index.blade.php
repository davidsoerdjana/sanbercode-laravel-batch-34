{{-- #81 --}}
@extends('layout.master')

@section('title')
Daftar Genre
@endsection

@section('content')

@auth
    <a href="/genre/create" class="btn btn-primary mb-3">Tambah Genre</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            @auth
                                <form action="/genre/{{$value->id}}" method="POST">
                                    <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                                    <a href="/genre/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                </form>
                            @endauth
                            @guest    
                                <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            @endguest
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection