{{-- #93 --}}
@extends('layout.master')

@section('title')
Detail Film
@endsection

@section('content')
<div class="row">
    
    <div class="col-12">
        <div class="card">
            <img src="{{asset('images/'.$film->poster)}}" alt="...">
            <div class="card-body">
              <h2><b>{{$film->judul}}</b></h2>
              <p class="card-text">{!! $film->ringkasan !!}</p>
              <a href="/film" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
    
</div>

{{-- *118 --}}
<h4><b>Kritik</b></h4>

@forelse ($film->kritik as $item)
    <div class="card">
        <div class="card-header">
            {{$item->user->name}}
        </div>
        <div class="card-body">
            <p class="card-text">Kritik: {{$item->content}}</p>
            <p class="card-text">Poin: {{$item->point}}</p>
        </div>
    </div>
@empty
    <h5>Tidak ada kritik</h5>
@endforelse

@auth
    <form action="/kritik" method="POST">
        @csrf
        <input type="hidden" value="{{$film->id}}" name="film_id">
        <textarea name="content" class="form-control" id="" placeholder="Kritik"></textarea>
        <input type="number" class="form-control" value="{{$film->id}}" name="point" placeholder="Poin">
        <input type="submit" value="Tambah kritik" class="btn btn-primary mt-3">
    </form>    
@endauth
{{-- *118 end --}}

@endsection