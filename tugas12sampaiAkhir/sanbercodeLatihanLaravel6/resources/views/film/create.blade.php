{{-- #89 --}}
@extends('layout.master')

@section('title')
Form Film Baru
@endsection

@section('content')
    <h2>Tambah Film</h2>
    {{-- pakai enctype="multipart/form-data" jika ingin upload data--}}
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="ringkasan">Ringkasan</label>
            <input type="text" class="form-control" name="ringkasan" id="ringkasan" placeholder="Masukkan Ringkasan">
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        {{-- *129 CKEDITOR --}}
        {{-- <label for="ringkasan">File Manager</label> --}}
        {{-- <textarea id="my-editor" name="ringkasan" class="form-control">{!! old('ringkasan', $ringkasan ?? '') !!}</textarea> <br> --}}
        {{-- lanjut ke bagian paling bawah --}}
        
        <div class="form-group">
            <label for="tahun">Tahun</label>
            <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="genre">Genre</label>
            <select name="genre_id" class="form-control">
                <option value="">---Pilih Genre---</option>
                @foreach ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
            @error('genre')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="poster">Poster</label>
            <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Poster">
            @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection

{{-- lanjutan *129 --}}
@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script>
<script>
    CKEDITOR.replace('my-editor', options);
</script>
@endpush
{{-- *129 end --}}