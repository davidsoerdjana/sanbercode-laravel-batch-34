{{-- #91 --}}
@extends('layout.master')

@section('title')
Daftar Film
@endsection

@section('content')

{{-- lanjutan *106 --}}
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah Film</a>
@endauth
{{-- lanjutan *106 --}}

<div class="row">
    @forelse ($film as $item)
    <div>
        <div class="card ml-3">
            
            <img src="{{asset('images/'.$item->poster)}}" width="100%">

            <div class="card-body col-md-12">

                <h3><b>{{$item->judul}}</b></h3>
                {{-- *115 --}}
                <span class="badge badge-primary">{{$item->genre->nama}}</span>
                {{-- *115 end--}}
                {{-- *130 --}}
                <p class="card-text">{!! Str::limit($item->ringkasan, 20) !!}</p>
                {{-- *130 end--}}

                {{-- lanjutan *106 --}}
                @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                        <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-primary btn-warning">Edit</a>
                        <input type="submit" class="btn btn-danger" value="Delete">
                </form>
                @endauth
                @guest
                    <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                @endguest
                {{-- *106 end --}}
                
            </div>
        
        </div>
    </div>
    @empty
        <h1>Tidak ada film</h1>
    @endforelse
</div>

{{-- <a href="/post/create" class="btn btn-primary mb-3">Tambah Film</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($post as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{{$value->body}}</td>
                        <td>
                            <form action="/post/{{$value->id}}" method="POST">
                            <a href="/post/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/post/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table> --}}
@endsection