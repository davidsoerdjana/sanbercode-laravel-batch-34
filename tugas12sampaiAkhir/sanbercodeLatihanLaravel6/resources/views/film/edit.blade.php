{{-- #95 --}}
@extends('layout.master')

@section('title')
Form Edit Film
@endsection

@section('content')
<h2>Edit Film</h2>
        {{-- pakai enctype="multipart/form-data" jika ingin upload data--}}
        <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" name="judul" value="{{$film->judul}}" id="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">Ringkasan</label>
                <input type="text" class="form-control" name="ringkasan" value="{{$film->ringkasan}}" id="ringkasan" placeholder="Masukkan Ringkasan">
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tahun">Tahun</label>
                <input type="text" class="form-control" name="tahun" value="{{$film->tahun}}" id="tahun" placeholder="Masukkan Tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="genre">Genre</label>
                <select name="genre_id" class="form-control">
                    <option value="">---Pilih Genre---</option>
                    
                    @foreach ($genre as $item)

                        @if ($item->id === $film->genre_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif    

                    @endforeach

                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="poster">Poster</label>
                <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Poster">
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection