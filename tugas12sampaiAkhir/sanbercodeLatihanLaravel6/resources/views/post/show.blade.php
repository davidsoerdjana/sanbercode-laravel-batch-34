{{-- #73 --}}
@extends('layout.master')

@section('title')
Detail Post
@endsection

@section('content')
<h2>Show Post {{$post->id}}</h2><br>
<h4>{{$post->title}}</h4>
<p>{{$post->body}}</p><br>
{{-- lanjutan #68 --}}
<p><b>Author: {{$post->author->name}}</b></p>
{{-- #68 end --}}

{{-- *135 --}}
<div>
  <b>Hashtag:
  @forelse ($post->tags as $tag)
    <button class="btn btn-primary btn-sm"> {{ $tag->tag_name}} </button>
    @empty
      No hashtag
  @endforelse
  </b>
</div>
{{-- *135 end --}}

@endsection