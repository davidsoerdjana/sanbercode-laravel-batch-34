{{-- #69 --}}
@extends('layout.master')

@section('title')
Form Post Baru
@endsection

@section('content')
<h2>Tambah Post</h2>
        <form action="/post" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value=" {{old('body', '')}} " placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <input type="text" class="form-control" name="body" id="body" value=" {{old('body', '')}} " placeholder="Masukkan Body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            {{-- lanjutan *133 dari Post.php --}}
            <div class="form-group">
                <label for="tags">Hashtag</label>
                <input type="text" class="form-control" name="tags" id="tags" value=" {{old('tags', '')}} " placeholder="Pisahkan dengan koma, contoh: viral, trending">
            </div>
            {{-- *133 end --}}
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        @endsection