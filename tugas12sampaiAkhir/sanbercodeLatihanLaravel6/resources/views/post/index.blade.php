{{-- #71 --}}
@extends('layout.master')

@section('title')
Daftar Post
@endsection

@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@auth
    <a href="/post/create" class="btn btn-primary mb-3">Tambah Post</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($post as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{{$value->body}}</td>
                        <td>
                            @auth
                                <form action="/post/{{$value->id}}" method="POST">
                                    <a href="/post/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                                    <a href="/post/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                </form>
                            @endauth
                            @guest
                                <a href="/post/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            @endguest
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection