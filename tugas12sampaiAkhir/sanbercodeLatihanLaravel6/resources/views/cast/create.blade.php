{{-- #53 --}}
@extends('layout.master')

@section('title')
Form Pemain Film Baru
@endsection

@section('content')

{{-- Referensi form https://getbootstrap.com/docs/4.6/components/forms/ --}}
<form method="POST" action="/cast">
  @csrf {{-- @csrf berfungsi untuk token, setiap metode post harus pakai @csrf --}}
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama">
    {{-- bisa juga pakai fitur dr HTML langsung jika tidak mau pakai @error, di dalam <input> setelah name="" tambahkan required  --}}
  </div>

  {{-- Referensi validasi https://laravel.com/docs/6.x/validation#main-content --}}
  {{-- @error untuk validasi jika kolom tidak diisi maka akan muncul pesan error --}}
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur">
  </div>

  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label>
    <textarea name="bio" class="form-control" cols="1" rows="3"></textarea>
  </div>

  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection