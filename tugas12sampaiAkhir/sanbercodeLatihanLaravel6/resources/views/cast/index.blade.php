{{-- #57 --}}
@extends('layout.master')

@section('title')
Daftar Pemain Film
@endsection

@section('content')

@auth    
  <a href="/cast/create/" class="btn btn-primary mb-3">Tambah Pemain</a>
@endauth

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      {{-- <th scope="col">Biodata</th> --}}
      <th scope="col">Detail</th>
    </tr>
  </thead>
  <tbody>

    {{-- @forelse adalah looping di dalam looping yg ada kondisi, jika data kosong makan akan tampil @empty --}}
    @forelse ($cast as $key => $item)
      <tr>
        <th>{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        {{-- <td>{{$item->bio}}</td> --}}
        <td>
          {{-- #65 --}}
          @auth
            <form action="/cast/{{$item->id}}" method="post">
              {{-- #61 --}}
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              {{-- #61 end --}}
              {{-- lanjutan #65 --}}
              @csrf
              @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          {{-- #65 end --}}
          @endauth
          @guest
          <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>    
          @endguest
        </td>
      </tr>
      @empty
        <tr>
          <td>Daftar cast masih kosong</td>
        </tr>
    @endforelse
    
  </tbody>
</table>

@endsection