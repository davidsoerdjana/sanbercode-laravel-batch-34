<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    {{-- code #19 --}}
    <a href="../../index3.html" class="brand-link">      
      <img src="{{asset('templateAdminLTE-3.2.0/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
         
          {{-- code #20 --}}
          <img src="{{('templateAdminLTE-3.2.0/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        
        </div>
        <div class="info">

          {{-- *107 fitur nama user--}}
          @auth
            <a href="#" class="d-block">{{ Auth::user()->name }} {{ Auth::user()->profile->umur }}</a>
          @endauth
          @guest
            <a href="#" class="d-block">Belum login</a>
          @endguest
          {{-- *107 end--}}

        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->              
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          
          {{-- lanjutan #91--}}
          {{-- code #22 --}}
          <li class="nav-item">
            <a href="/film" class="nav-link">
              {{-- code #23 --}}
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </a>
          </li>
          {{-- code #22 end --}}
          {{-- lanjutan #91 end--}}

          {{-- lanjutan *81 --}}
          <li class="nav-item">
            <a href="/genre" class="nav-link">
              <i class="nav-icon fas fa-film"></i>
              <p>
                Genre
              </p>
            </a>
          </li>
          {{-- lanjutan *81 end--}}

          {{-- lanjutan *57 --}}
          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Cast
              </p>
            </a>
          </li>
          {{-- lanjutan *57 end--}}
          
          <li class="nav-item">
            {{-- code #28 --}}
            <a href="/data-tables" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              {{-- code #27 --}}
              <p>Table</p>
            </a>
          </li>

          {{-- code #71 --}}
          <li class="nav-item">
            <a href="/post" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Post
              </p>
            </a>
          </li>
          {{-- code #71 end--}}

          {{-- code #21 --}}
          <li class="nav-item">
            <a href="" class="nav-link">
              {{-- code #25 --}}
              <i class="nav-icon fas fa-th"></i>
              {{-- code #24 --}}
              <p>
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">  
              {{-- *108 --}}
              <li class="nav-item">
                <a href="/profile" class="nav-link">
                  {{-- <i class="nav-icon fas fa-user"></i> --}}
                  <p>
                    Profile
                  </p>
                </a>
              </li>
              {{-- *108 end--}}
            </ul>
          </li>
          

            {{-- *104 --}}
            @auth
              {{-- halaman yang ingin diarahkan ke login masukkan disini --}}
            @endauth
            {{-- *104 end --}}

              {{-- code #26 --}}
              {{-- <li class="nav-item">
                <a href="../../index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li> --}}
              {{-- code #26 end --}}

          </li>
          {{-- code #21 end --}}

          {{-- *102 --}}
          @auth
            <li class="nav-item btn-danger">
              <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Logout
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </li>
          @endauth
          {{-- *102 end --}}
          
          {{-- *105 --}}
          @guest
            <li class="nav-item bg-info">
              <a class="nav-link" href="/login">
                Login
              </a>
            </li>
          @endguest
          {{-- *105 end--}}

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>