{{-- *111 --}}
@extends('layout.master')

@section('title')
Halaman Edit Profile
@endsection

@section('content')


{{-- Referensi form https://getbootstrap.com/docs/4.6/components/forms/ --}}
<form method="POST" action="/profile/{{$profile->id}}">
 
  @csrf {{-- @csrf berfungsi untuk token, setiap metode post harus pakai @csrf --}}
  @method('put')

  {{-- *113 --}}
  <div class="form-group">
    <label>Nama User</label>
    <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
  </div>
  <div class="form-group">
    <label>Email User</label>
    <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
  </div>
  {{-- *113 end--}}

  <div class="form-group">
    <label>Umur</label>
    <input type="number" class="form-control" value="{{$profile->umur}}" name="umur">
    {{-- bisa juga pakai fitur dr HTML langsung jika tidak mau pakai @error, di dalam <input> setelah name="" tambahkan required  --}}
  </div>
  {{-- Referensi validasi https://laravel.com/docs/6.x/validation#main-content --}}
  {{-- @error untuk validasi jika kolom tidak diisi maka akan muncul pesan error --}}
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label>
    <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
  </div>
  @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection