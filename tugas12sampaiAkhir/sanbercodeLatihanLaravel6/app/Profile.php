<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    // *100
    protected $table = "profile"; // protected $table berfungsi mengarahkan model ke table yg dituju di database
    protected $fillable = ["users_id", "umur", "bio", "alamat"]; // protected $fillable berfungsi kolom apa saja yang akan di manipulasi

    // *112
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    // lanjut ke User.php
}
