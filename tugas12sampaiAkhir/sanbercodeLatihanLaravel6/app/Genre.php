<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    // lanjutan #77
    protected $table = "genre"; // protected $table berfungsi mengarahkan model ke table yg dituju di database
    protected $fillable = ["nama"]; // protected $fillable berfungsi Kolom apa saja yang akan di manipulasi
    // #77 end

    // *114
    public function film()
    {
        return $this->hasMany('App\Film');
    }
    
}
