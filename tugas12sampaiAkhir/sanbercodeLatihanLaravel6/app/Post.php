<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // lanjutan #67 dari migration post
    protected $table = "post"; // protected $table berfungsi mengarahkan model ke table post di database
    protected $fillable = ["users_id", "title", "body"]; // protected $fillable berfungsi Kolom apa saja yang akan di manipulasi
    // #67 end

    // lanjutan #68 dari PostController.php
    public function author()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    // lanjut ke User.php

    // *133
    public function tags() // many to many relationships
    {
        return $this->belongsToMany('App\Tag', 'post_tag', 'post_id', 'tag_id');
    }
    // lanjut ke create.blade.php
}
