<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    // *117
    protected $table = "kritik"; // protected $table berfungsi mengarahkan model ke table yg dituju di database
    protected $fillable = ["users_id", "film_id", "content", "point"]; // protected $fillable berfungsi kolom apa saja yang akan di manipulasi
    
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function film()
    {
        return $this->belongsTo('App\Film', 'film_id');
    }
    // lanjut ke User.php dan Film.php
}
