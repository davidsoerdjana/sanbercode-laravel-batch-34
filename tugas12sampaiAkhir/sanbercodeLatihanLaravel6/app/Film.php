<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    // lanjutan #67
    protected $table = "film"; // protected $table berfungsi mengarahkan model ke table yg dituju di database
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"]; // protected $fillable berfungsi Kolom apa saja yang akan di manipulasi
    // #67 end

    // lanjutan *114
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genre_id');
    }
    // *114 end

    // lanjutan dari Kritik.php *117
    public function kritik()
    {
        return $this->hasMany('App\Kritik');
    }
    // lanjut ke User.php
}
