<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() // code #5
    {
        return view('page.register');
    }

    public function welcome(request $request) // code #8
    {
        // // dd digunakan untuk menampilkan data
        // dd($request->all());
        // dd($request["first_name"]);

        $namaDepan = $request['first_name'];
        $namaBelakang = $request['last_name'];

        // return "$namaDepan";
        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }
}
