<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// #88
use Illuminate\Support\Facades\Auth;
use App\Film; // berfungsi import model yang berada di direktori folder app
use App\Genre;

// #94
use File;

// lanjutan *120 dari .env
use RealRashid\SweetAlert\Facades\Alert;
// lanjut ke function store

class FilmController extends Controller
{
    // *106 middleware
     public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    // *106 end

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // jika dd($film) di atas $film = Film::all maka di localhost muncul data array eloquent
        // jika dd($film) di bawah $film = DB::table maka di localhost muncul data array biasa
        // dd($film);

        // $film = DB::table('film')->get(); // kalau di query builder itu select * from film

        // #90
        // fitur eloquent dengan method all() untuk mengambil semua data table film
        $film = Film::all();

        return view('film.index', compact('film'));
        // #90 end
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // lanjutan #88
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // lanjutan #88
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048|'
        ]);
        
        // fungsinya menkonversi nama file agar tidak ada yg sama saat masuk ke database
        // referensi: https://www.itsolutionstuff.com/post/laravel-6-image-upload-tutorialexample.html
        $imageName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('images'), $imageName);

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $imageName;
        $film->save();

        // lanjutan *120 dari atas
        Alert::success('Berhasil', 'Yeayy, kamu berhasil menambahkan film');
        // *120 end

        return redirect('/film');
        // #88 end
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // #92
        // fitur eloquent dengan method find() berfungsi mengambil semua data berdasarkan variabel id $id di database
        $film = Film::find($id);
        return view('film.show', compact('film'));
        // #92 end
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // lanjutan #94
        $film = Film::find($id);
        $genre = Genre::all();

        return view('film.edit', compact('film', 'genre'));
    }

    public function update($id, Request $request)
    {
        // lanjutan #94
        // fitur eloquent dengan method update() berfungsi mengubah data bedasarkan method find($id) ke database
        $request->validate([
            // 'title' => 'required|unique:film',
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048|'
        ]);

        $film = Film::find($id);

        if ($request->has('poster')) {
            $path = "images/";
            File::delete($path . $film->poster);
            $imageName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('images'), $imageName);
            $film->poster = $imageName;
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->save();
        return redirect('/film');
        // #94 end
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     // 
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // #96
        // fitur eloquent dengan method delete() berfungsi menghapus data bedasarkan method find($id) ke database
        $film = Film::find($id);
        $film->delete();

        $path = "images/";
        File::delete($path . $film->poster);
        
        return redirect('/film');

        // cara simple delete data dengan model (eloquent)
        // Film::destroy($id);
        // return redirect('/film')->with('Success', 'Data berhasil dihapus!');
        
        // #96 end
    }
}
