<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

#78
use App\Genre; // berfungsi import model yang berada di direktori folder app
// lanjut ke create di bawah

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // jika dd($genre) di atas $genre = Genre::all maka di localhost muncul data array eloquent
        // jika dd($genre) di bawah $genre = DB::table maka di localhost muncul data array biasa
        // dd($genre);

        // $genre = DB::table('genre')->get(); // kalau di query builder itu select * from genre

        // #80
        // fitur eloquent dengan method all() untuk mengambil semua data table genre
        $genre = Genre::all();

        return view('genre.index', compact('genre'));
        // #80 end
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // lanjutan #78
        return view('genre.create');
        // lanjut ke store di bawah
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // lanjutan #78
        $this->validate($request,[
    		'nama' => 'required'
    	]);
        
        // fitur eloquent dengan method create() berfungsi melakukan penginputan data ke database
        Genre::create([
    		'nama' => $request->nama
    	]);
 
    	return redirect('/genre');
        // #78 end
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // #82
        // fitur eloquent dengan method find() berfungsi mengambil semua data berdasarkan variabel id $id di database
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
        // #82 end
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // #84
        $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request)
    {
        // lanjutan #84
        // fitur eloquent dengan method update() berfungsi mengubah data bedasarkan method find($id) ke database
        $request->validate([
            // 'title' => 'required|unique:genre',
            'nama' => 'required'
        ]);

        $genre = Genre::find($id);
        $genre->nama = $request->nama;
        $genre->update();
        return redirect('/genre');
        // #84 end
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     // 
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // #86
        // fitur eloquent dengan method delete() berfungsi menghapus data bedasarkan method find($id) ke database
        $genre = Genre::find($id);
        $genre->delete();
        return redirect('/genre');

        // cara simple delete data dengan model (eloquent)
        // Genre::destroy($id);
        // return redirect('/genre')->with('Success', 'Data berhasil dihapus!');
        
        // #86 end
    }
}
