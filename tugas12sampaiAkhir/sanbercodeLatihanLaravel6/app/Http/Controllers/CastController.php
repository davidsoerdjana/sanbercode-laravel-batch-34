<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // #54 untuk pakai fungsi DB di laravel

class CastController extends Controller
{
    // #52
    public function create() 
    {
        return view('cast.create');
    }

    public function store(request $request)
    {
        // // dd digunakan untuk menampilkan data
        // dd($request->all());

        // Referensi validasi https://laravel.com/docs/6.x/validation#main-content
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'nama harus diisi!',
            'nama.min' => 'nama harus lebih dari 5 karakter!',
            'umur.required' => 'umur harus diisi!',
            'bio.required' => 'biodata harus diisi!'
        ]);

        // lanjutan #54
        // jika saat post ke table cast muncul pesan error, bisa restart php artisan serve
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
        // #54 end
    }
    // #52 end

    // #56
    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', compact('cast'));
    }
    // #56 end

    // #60
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($id, request $request)
    {
        $request->validate([
            'nama' => 'required|min:5]',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'nama harus diisi!',
            'nama.min' => 'nama harus lebih dari 5 karakter!',
            'umur.required' => 'umur harus diisi!',
            'bio.required' => 'biodata harus diisi!'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }
    // #60 end

    // #64
    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
    // #64 end
}
