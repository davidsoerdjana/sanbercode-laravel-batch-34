<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// *109
use Illuminate\Support\Facades\Auth;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('users_id', Auth::id())->first();
        // dd($profile); // di browser ketik http://localhost:8000/profile
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ],
        [
            'umur.required' => 'umur harus diisi!',
            'bio.required' => 'biodata harus diisi!',
            'alamat.required' => 'alamat harus diisi!'
        ]);

        $profile = Profile::find($id);
        
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;

        $profile->save();

        return redirect('/profile');
    }
    // *109 end
}
