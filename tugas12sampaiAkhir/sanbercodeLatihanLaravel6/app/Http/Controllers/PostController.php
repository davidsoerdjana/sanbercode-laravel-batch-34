<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// #68
use Illuminate\Support\Facades\Auth;
use DB;
use App\Post; // berfungsi import model yang berada di direktori folder app
// lanjut ke create di bawah

// *134
use App\Tag;
// lanjut ke store di bawah

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // jika dd($post) di atas $post = Post::all maka di localhost muncul data array eloquent
        // jika dd($post) di bawah $post = DB::table maka di localhost muncul data array biasa
        // dd($post);

        // $post = DB::table('post')->get(); // kalau di query builder itu select * from post

        // #70
        // fitur eloquent dengan method all() untuk mengambil semua data table post
        $post = Post::all();

        // $user = Auth::id(); // mengambil user yang sedang login
        // $user = $user->post; // mengambil postingan user yang sedang login saja
        
        return view('post.index', compact('post'));
        // #70 end
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // lanjutan #68
        return view('post.create');
        // lanjut ke store di bawah
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // lanjutan #68
        $this->validate($request,[
    		'title' => 'required|unique:post',
    		'body' => 'required'
    	]); // lanjut ke bawah


        // *134 lanjutan dari atas. Input hashtag:
        $tags_arr = explode(',', $request["tags"]); // explode untuk mengubah request tags jadi array
        $tags_ids = []; // buat array penampung
        foreach ($tags_arr as $tag_name) { // looping ke array tags yang sudah diubah dengan explode tadi
            $tag = Tag::where("tag_name", $tag_name)->first();
            if($tag) { // setiap kali looping melakukan pengecekan apakah sudah ada tag nya, kalau sudah ada maka ambil id nya
                $tag_ids[] = $tag->id;
            } else { // kalau belum ada maka simpan dulu tag nya lalu ambil id nya
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        } // lanjut ke bawah
        // // dd($tag_ids);
        
        
        // // lanjutan dari atas *134. Cara lain input hashtag method firstOrCreate:
        // $tags_arr = explode(',', $request["tags"]);
        // // dd($tags_arr);
        // $tags_ids = [];
        // foreach ($tags_arr as $tag_name) {
        //     $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
        //     $tag_ids[] = $tag->id;
        // }


        // lanjutan #68
        $post = new Post;
        $post->title = $request->title;
        $post-> body = $request->body;

        // lanjutan *134 dari atas
        // $post->tags()->sync($tag_ids); // metode snyc menghubungkan table post dan tags(many to many) 
        // lanjut ke Tag.php

        // lanjutan #68
        $post->users_id = Auth::id();
        $post->save();
        // lanjut ke bawah


        // // query builder method create
        // $query = DB::table('post')->insert({
        //     "title" => $request["title"],
        //     "body" => $request["body"]
        // });


        // // fitur eloquent dengan method create() berfungsi melakukan penginputan data ke database
        // Post::create([
    	// 	'title' => $request->title,
    	// 	'body' => $request->body
    	// ]);


        // // cara lain fitur eloquent dengan method create()
        // $post = Post::create([
        //     "title" => $request["title"],
        //     "body" => $request["body"],
        // ]);


        // // fitur eloquent dengan method create() untuk melakukan penginputan data ke database ke id yang login saja
        // $post = $user->post()->create([
        //     "title" => $request["title"],
        //     "body" => $request["body"],
        // ]);
        // $user = Auth::user();
        // $user->post()->save($post);


        // #68 lanjutan dari atas
    	return redirect('/post')->with('success', 'Post berhasil disimpan!');
        // #68 lanjut ke Post.php
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // #72
        // fitur eloquent dengan method find() berfungsi mengambil semua data berdasarkan variabel id $id di database
        $post = Post::find($id);
        return view('post.show', compact('post'));
        // #72 end
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // #74
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        // lanjutan #74
        // fitur eloquent dengan method update() berfungsi mengubah data bedasarkan method find($id) ke database
        $request->validate([
            // 'title' => 'required|unique:post',
            'title' => 'required',
            'body' => 'required'
        ]);

        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->update();
        return redirect('/post');
        // #74 end
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     // 
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // #76
        // fitur eloquent dengan method delete() berfungsi menghapus data bedasarkan method find($id) ke database
        $post = Post::find($id);
        $post->delete();
        return redirect('/post');

        // cara simple delete data dengan model (eloquent)
        // Post::destroy($id);
        // return redirect('/post')->with('Success', 'Data berhasil dihapus!');
        
        // #76 end
    }
}
