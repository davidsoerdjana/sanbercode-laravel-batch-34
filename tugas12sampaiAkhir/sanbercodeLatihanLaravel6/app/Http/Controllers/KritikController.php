<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// *119
use Illuminate\Support\Facades\Auth;
use App\Kritik; // berfungsi import model yang berada di direktori folder app

class KritikController extends Controller
{
    public function store(request $request){
        $kritik = new Kritik;
        $kritik->content = $request->content;
        $kritik->point = $request->point;
        $kritik->film_id = $request->film_id;
        $kritik->users_id = Auth::id();
        $kritik->save();

        return redirect('/film/'. $request->film_id);
    }
    // *119 end
}
