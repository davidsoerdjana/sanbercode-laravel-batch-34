<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// *127
use App\Exports\ExcelsExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    // lanjutan *127
    public function export() 
    {
        return Excel::download(new ExcelsExport, 'excels.xlsx');
    }
    // lanjut ke web.php
}
