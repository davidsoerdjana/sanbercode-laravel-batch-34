<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // lanjutan dari Profile.php *112
    public function profile()
    {
        return $this->hasOne('App\Profile', 'users_id');
    } // *112 end

    // lanjutan dari Kritik.php *117
    public function kritik()
    {
        return $this->hasMany('App\Kritik');
    } // *117 end

    // lanjutan #68
    public function post()
    {
        return $this->hasMany('App\Post');
    }
    // lanjut ke show.blade.php
}
