<?php

namespace App\Exports;

use App\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExcelsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Excel::all();
    }
}
