<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// // route default
// Route::get('/', function () {
//     return view('welcome');
// });

// // cara lain penulisan route, jika tidak mau pakai controller maka pakai function
// Route::get('/test/{angka}', function ($angka) {
//     return view('test', ["angka" => $angka]);
// });

// // juga bisa seperti ini
// Route::get('/halo/{nama}', function ($angka) {
//     return "halo $nama";
// });


// Route::get('/', 'AuthController@home'); // code #1
// Route::get('/register', 'AuthController@register'); // code #4
// Route::post('/welcome', 'AuthController@welcome'); // code #7


// Route::get('/master', function () { // code #10
//     return view('layout/master');
// });


// Route::get('/', function () {
//     return view('page.home');
// });

Route::get('/', 'FilmController@index')->name('index');
Route::get('/home', 'FilmController@index')->name('index');


// code #36
Route::get('/table', function () {
    return view('page.table');
});


// *103
Auth::routes();

// middleware, fungsinya jika klik localhost:8000/data-tables maka akan dialihkan ke halaman login
Route::group(['middleware' => ['auth']], function () {
    
    
    // lanjutan code #36
    Route::get('/data-tables', function () {
        return view('page.data-tables');
    });
    // code #36 end

    // *110
    Route::resource('profile', 'ProfileController')->only(
        'index', 'update'
    );
    // *110 end
    
    // *116
    Route::resource('kritik', 'KritikController')->only(
        'store'
    );
    // *116 end

});
// *103 end


// CRUD cast
// Create
// #51
Route::get('/cast/create', 'CastController@create'); // route untuk menuju ke form pemain film baru
Route::post('/cast', 'CastController@store'); // route untuk menyimpan data ke database

// Read
// #55
Route::get('/cast', 'CastController@index'); // route untuk menampilkan semua cast, ambil dari database
Route::get('/cast/{cast_id}', 'CastController@show'); // route untuk detail cast berdasarkan cast_id

// Update
// #59
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route untuk mengarah ke form edit pemain film
Route::put('/cast/{cast_id}', 'CastController@update'); // route update data berdasarkan id

// Delete
// #63
Route::delete('/cast/{cast_id}/', 'CastController@destroy');


// CRUD post, pakai resource langsung dibuatkan template untuk CRUD nya. Cek list route di CMD
// #66
Route::resource('post', 'PostController');
// #66 end


// CRUD genre
// #77
Route::resource('genre', 'GenreController');
// #77 end


// CRUD film    
// #87
Route::resource('film', 'FilmController');
// #87 end


// *122
// library DOMPDF
// cara 1: langsung ke browser http://localhost:8000/test-dompdf
// Route::get('/test-dompdf', function(){
//     $pdf = App::make('dompdf.wrapper');
//     $pdf->loadHTML('<h1>Test</h1>');
//     return $pdf->stream();
// });

// cara 2: pakai blade
Route::get('/test-dompdf2', 'PdfController@test');
// *122 end


// library laravel excel
// lanjutan *127 dari ExcelController
Route::get('test-excel', 'ExcelController@export');
// *127 end


// *128
// library file manager by unisharp
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });
 // *128 end