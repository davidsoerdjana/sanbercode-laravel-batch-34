<?php

// soal 13, screenshot (19)
echo "Jawaban soal 13:" . "<br>";
function ubah_huruf($string) {
  $abjad = "abcdefghijklmnopqrstuvwxyz";
  $output = ""; // variabel tampung
    for ($a = 0; $a < strlen($string); $a++) {
      $position = strpos($abjad, $string[$a]);
      $output .= substr($abjad, $position + 1, 1);
    }
    return $output . "<br>";
}

// TEST CASES jika outputnya banyak sperti ini maka harus pake looping
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat') . "<br>"; // tfnbohbu


// soal 14, screenshot (20)
echo "Jawaban soal 14:" . "<br>";
function tukar_besar_kecil($string) {
  $hasil = ""; // variabel tampung
    for ($i = 0; $i < strlen($string); $i++) {
      if (ctype_lower($string[$i])) {
        $hasil .= strtoupper($string[$i]);
      } else {
        $hasil .= strtolower($string[$i]);
      }
    }
    return $hasil . "<br>";
}

// TEST CASES jika outputnya banyak sperti ini maka harus pake looping
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"