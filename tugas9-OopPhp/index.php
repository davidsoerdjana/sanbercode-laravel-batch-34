<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->namaBinatang . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br> <br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->namaBinatang . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump("Hop Hop") . "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->namaBinatang . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell("Auooo");
