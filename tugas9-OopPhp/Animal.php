<?php

class Animal
{
  public $namaBinatang;
  public $legs = 4;
  public $cold_blooded = "no";

  public function __construct($nama)
  {
    $this->namaBinatang = $nama;
  }
}
